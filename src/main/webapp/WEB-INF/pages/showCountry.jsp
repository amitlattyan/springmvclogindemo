<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Spring MVC List of objects display</title>
</head>
<body>
<table>
<tr>
<th>CountryName</th>
<th>TopLevelDomain</th>
<th>Alpha2Code</th>
<th>Alpha3Code</th>
<th>CallingCodes</th>
<th>Capital</th>
<th>AltSpellings</th>
<th>Region</th>
<th>Subregion</th>
<th>Population</th>
<th>LatLng</th>
<th>Demonym</th>
<th>Area</th>
<th>Gini</th>
<th>Timezones</th>
<th>Borders</th>
<th>NativeName</th>
<th>NumericCode</th>
<th>Currencies</th>
<th>Languages</th>
<th>Relevance</th>
<th>Translations</th>
</tr>
<c:forEach items="${CountryList.countaryDetails}" var="country" varStatus="tagStatus">
    <tr>
        <td>${country.name}</td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="arr" items="${country.topLevelDomain}">
				            <td>${arr} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>${country.alpha2Code}</td>
        <td>${country.alpha3Code}</td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="codes" items="${country.callingCodes}">
				            <td>${codes} </td>
				        </c:forEach>
			        </tr>
			</table>
        </td>
        <td>${country.capital}</td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="spl" items="${country.altSpellings}">
				            <td>${spl} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>${country.region}</td>
        <td>${country.subregion}</td>
        <td>${country.population}</td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="lat" items="${country.latlng}">
				            <td>${lat} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>${country.demonym}</td>
        <td>${country.area}</td>
        <td>${country.gini}</td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="tzon" items="${country.timezones}">
				            <td>${tzon} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="bdr" items="${country.borders}">
				            <td>${bdr} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>${country.nativeName}</td>
        <td>${country.numericCode}</td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="curre" items="${country.currencies}">
				            <td>${curre} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>
	        <table>
			        <tr>
				        <c:forEach var="lng" items="${country.languages}">
				            <td>${lng} </td>
				        </c:forEach>
			        </tr>
	        </table>
        </td>
        <td>${country.relevance}</td>
        <td>${country.translation}</td>
        
    </tr>
</c:forEach>
</table>
</body>
</html>