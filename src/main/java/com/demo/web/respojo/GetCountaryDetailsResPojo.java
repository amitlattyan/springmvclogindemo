package com.demo.web.respojo;

import java.util.ArrayList;

import com.demo.web.models.CountaryDetails;

public class GetCountaryDetailsResPojo {
	private ArrayList<CountaryDetails> countaryDetails;

	public ArrayList<CountaryDetails> getCountaryDetails() {
		return countaryDetails;
	}

	public void setCountaryDetails(ArrayList<CountaryDetails> countaryDetails) {
		this.countaryDetails = countaryDetails;
	}

	@Override
	public String toString() {
		return "GetCountaryDetailsResPojo [countaryDetails=" + countaryDetails + "]";
	}

}
