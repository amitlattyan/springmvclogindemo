package com.demo.web.dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import java.lang.reflect.Type;

import com.demo.web.models.CountaryDetails;


@Service
public class GetCountaryDetailsDaoImpl implements GetCountaryDetailsDao {

	public static void main(String args[]) {
		GetCountaryDetailsDaoImpl get = new GetCountaryDetailsDaoImpl();
		get.getListOfCountry();
	}
	
	@Override
	public List<CountaryDetails> getListOfCountry() {
		
         List<CountaryDetails> list = new ArrayList<CountaryDetails>();
		
		//CountaryDetails countaryDetails = new CountaryDetails();
		//Gson gson = new Gson();
		
		try {
			URL url = new URL("https://restcountries.eu/rest/v1/all");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			
			/*String response = "";
			String line = "";
			while ((line = br.readLine()) != null) {
				response = response +" "+ line; 
			}
			
			response="{\"master\":"+response+"}";
			countaryDetails = gson.fromJson(response, CountaryDetails.class);
			
			Type type = new TypeToken<List<CountaryDetails>>() {
			}.getType();
			list = gson.fromJson(response, type);*/
            
			Object obj = new JSONParser().parse(br);
			JSONArray jsonarr_1 = (JSONArray) obj;

			for (int i = 0; i < jsonarr_1.size(); i++) {
				
				CountaryDetails det = new CountaryDetails();
				JSONObject jsonobj_1 = (JSONObject) jsonarr_1.get(i);
				
				//System.out.println("Hi name: "+ i +" " + jsonobj_1.get("name"));
				
				det.setName(jsonobj_1.get("name").toString());
				
				JSONArray topleveldomain = (JSONArray) jsonobj_1.get("topLevelDomain");
				String[] dom=new String[topleveldomain.size()];
			    for(int j=0; j<dom.length; j++) {
			    	dom[j]=(String) topleveldomain.get(j);
			    }
				det.setTopLevelDomain(dom);
				
				det.setAlpha2Code(jsonobj_1.get("alpha2Code").toString());
				det.setAlpha3Code(jsonobj_1.get("alpha3Code").toString());
				
				JSONArray callingcode = (JSONArray) jsonobj_1.get("callingCodes");
				String[] callingCodes=new String[callingcode.size()];
			    for(int j=0; j<callingCodes.length; j++) {
			    	callingCodes[j]=(String) callingcode.get(j);
			    }
				det.setCallingCodes(callingCodes);
				
				det.setCapital(jsonobj_1.get("capital").toString());
				
				JSONArray altspl = (JSONArray) jsonobj_1.get("altSpellings");
				String[] altSpellings=new String[altspl.size()];
			    for(int j=0; j<altSpellings.length; j++) {
			    	altSpellings[j]=(String) altspl.get(j);
			    }
			    det.setAltSpellings(altSpellings);
			    
			    det.setRegion(jsonobj_1.get("region").toString());
			    det.setSubregion(jsonobj_1.get("subregion").toString());
			    det.setPopulation(jsonobj_1.get("population")+"");

			    JSONArray latlng = (JSONArray) jsonobj_1.get("latlng");
				double[] latlngs=new double[latlng.size()];
			    for(int j=0; j<latlngs.length; j++) {
			    	latlngs[j]=(Double) latlng.get(j);
			    }
			    det.setLatlng(latlngs);
			    det.setDemonym(jsonobj_1.get("demonym").toString());
			    det.setArea(jsonobj_1.get("area")+"");
			    det.setGini(jsonobj_1.get("gini")+"");
			    
			    JSONArray timezone = (JSONArray) jsonobj_1.get("timezones");
				String[] timezones=new String[timezone.size()];
			    for(int j=0; j<timezones.length; j++) {
			    	timezones[j]=(String) timezone.get(j);
			    }
			    det.setTimezones(timezones);
			    
			    JSONArray border = (JSONArray) jsonobj_1.get("borders");
				String[] borders=new String[border.size()];
			    for(int j=0; j<borders.length; j++) {
			    	borders[j]=(String) border.get(j);
			    }
			    det.setBorders(borders);
			    det.setNativeName(jsonobj_1.get("nativeName")+"");
			    det.setNumericCode(jsonobj_1.get("numericCode")+"");
			    
			    JSONArray currencie = (JSONArray) jsonobj_1.get("currencies");
				String[] currencies=new String[currencie.size()];
			    for(int j=0; j<currencies.length; j++) {
			    	currencies[j]=(String) currencie.get(j);
			    }
			    det.setCurrencies(currencies);
			    
			    JSONArray language = (JSONArray) jsonobj_1.get("currencies");
				String[] languages=new String[language.size()];
			    for(int j=0; j<languages.length; j++) {
			    	languages[j]=(String) language.get(j);
			    }
			    det.setLanguages(languages);
			    det.setRelevance(jsonobj_1.get("relevance")+"");
			    
			    JSONObject translations = (JSONObject) jsonobj_1.get("translations");
			    String trans = "de="+translations.get("de")+", es="+translations.get("es")+", fr="+translations.get("fr")+", ja="+translations.get("ja")+", it="+translations.get("it");
			    det.setTranslation(trans);
			    
			    
				list.add(det);
			}

		} catch (Exception e) {
			System.out.println("Exception " + e);
		}
		return list;
	}

}
