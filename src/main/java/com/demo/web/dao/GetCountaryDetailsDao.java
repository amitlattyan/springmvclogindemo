package com.demo.web.dao;

import java.util.List;

import com.demo.web.models.CountaryDetails;

public interface GetCountaryDetailsDao {
	
	public List<CountaryDetails> getListOfCountry();

}
