package com.demo.web.models;

import java.util.Arrays;

public class CountaryDetails {
	
	private String name;
	private String topLevelDomain[];
	private String alpha2Code;
	private String alpha3Code;
	private String callingCodes[];
	private String capital;
	private String altSpellings[];
	private String region;
	private String subregion;
	private String population;
	private double latlng[];
	private String demonym;
	private String area;
	private String gini;
	private String timezones[];
	private String borders[];
	private String nativeName;
	private String numericCode;
	private String currencies[];
	private String languages[];
	private String translation;
	private String relevance;
	private translations translations;
	

	public String getTranslation() {
		return translation;
	}


	public void setTranslation(String translation) {
		this.translation = translation;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	

	public String[] getTopLevelDomain() {
		return topLevelDomain;
	}


	public void setTopLevelDomain(String[] topLevelDomain) {
		this.topLevelDomain = topLevelDomain;
	}


	public String getAlpha2Code() {
		return alpha2Code;
	}


	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}


	public String getAlpha3Code() {
		return alpha3Code;
	}


	public void setAlpha3Code(String alpha3Code) {
		this.alpha3Code = alpha3Code;
	}


	public String[] getCallingCodes() {
		return callingCodes;
	}


	public void setCallingCodes(String[] callingCodes) {
		this.callingCodes = callingCodes;
	}


	public String getCapital() {
		return capital;
	}


	public void setCapital(String capital) {
		this.capital = capital;
	}


	public String[] getAltSpellings() {
		return altSpellings;
	}


	public void setAltSpellings(String[] altSpellings) {
		this.altSpellings = altSpellings;
	}


	public String getRegion() {
		return region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public String getSubregion() {
		return subregion;
	}


	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}


	public String getPopulation() {
		return population;
	}


	public void setPopulation(String population) {
		this.population = population;
	}


	public double[] getLatlng() {
		return latlng;
	}


	public void setLatlng(double[] latlng) {
		this.latlng = latlng;
	}


	public String getDemonym() {
		return demonym;
	}


	public void setDemonym(String demonym) {
		this.demonym = demonym;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getGini() {
		return gini;
	}


	public void setGini(String gini) {
		this.gini = gini;
	}


	public String[] getTimezones() {
		return timezones;
	}


	public void setTimezones(String[] timezones) {
		this.timezones = timezones;
	}


	public String[] getBorders() {
		return borders;
	}


	public void setBorders(String[] borders) {
		this.borders = borders;
	}


	public String getNativeName() {
		return nativeName;
	}


	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}


	public String getNumericCode() {
		return numericCode;
	}


	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}


	
	public String[] getCurrencies() {
		return currencies;
	}


	public void setCurrencies(String[] currencies) {
		this.currencies = currencies;
	}


	

	public String[] getLanguages() {
		return languages;
	}


	public void setLanguages(String[] languages) {
		this.languages = languages;
	}


	public translations getTranslations() {
		return translations;
	}


	public void setTranslations(translations translations) {
		this.translations = translations;
	}


	public String getRelevance() {
		return relevance;
	}


	public void setRelevance(String relevance) {
		this.relevance = relevance;
	}


	public class translations{
		private String de;
		private String es;
		private String fr;
		private String ja;
		private String it;
		public String getDe() {
			return de;
		}
		public void setDe(String de) {
			this.de = de;
		}
		public String getEs() {
			return es;
		}
		public void setEs(String es) {
			this.es = es;
		}
		public String getFr() {
			return fr;
		}
		public void setFr(String fr) {
			this.fr = fr;
		}
		public String getJa() {
			return ja;
		}
		public void setJa(String ja) {
			this.ja = ja;
		}
		public String getIt() {
			return it;
		}
		public void setIt(String it) {
			this.it = it;
		}
		@Override
		public String toString() {
			return "translations [de=" + de + ", es=" + es + ", fr=" + fr + ", ja=" + ja + ", it=" + it + "]";
		}
	}
	

	
	@Override
	public String toString() {
		return "CountaryDetails [name=" + name + ", topLevelDomain=" + Arrays.toString(topLevelDomain) + ", alpha2Code="
				+ alpha2Code + ", alpha3Code=" + alpha3Code + ", callingCodes=" + Arrays.toString(callingCodes)
				+ ", capital=" + capital + ", altSpellings=" + Arrays.toString(altSpellings) + ", region=" + region
				+ ", subregion=" + subregion + ", population=" + population + ", latlng=" + Arrays.toString(latlng)
				+ ", demonym=" + demonym + ", area=" + area + ", gini=" + gini + ", timezones="
				+ Arrays.toString(timezones) + ", borders=" + Arrays.toString(borders) + ", nativeName=" + nativeName
				+ ", numericCode=" + numericCode + ", currencies=" + Arrays.toString(currencies) + ", languages="
				+ Arrays.toString(languages) + ", translation=" + translation + ", relevance=" + relevance
				+ ", translations=" + translations + "]";
	}

	

}
