package com.demo.web.models;

import java.util.List;

public class CountryListContainer {
	
	private List<CountaryDetails> countaryDetails;

	public List<CountaryDetails> getCountaryDetails() {
		return countaryDetails;
	}

	public void setCountaryDetails(List<CountaryDetails> countaryDetails) {
		this.countaryDetails = countaryDetails;
	}

	@Override
	public String toString() {
		return "CountryListContainer [countaryDetails=" + countaryDetails + "]";
	}
	

}
