package com.demo.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.demo.web.dao.GetCountaryDetailsDaoImpl;
import com.demo.web.models.CountaryDetails;
import com.demo.web.models.CountryListContainer;

@Controller
public class HelloController {
	
	@Autowired
    private GetCountaryDetailsDaoImpl countryDAOImpl;
	
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView welcome() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "My Page");
		model.addObject("message", "Welcome Page");
		model.setViewName("login");
		return model;

	}

	@RequestMapping(value = { "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView welcomePage() {

		ModelAndView model = new ModelAndView();
		List<CountaryDetails> countaryDetails = countryDAOImpl.getListOfCountry();
        CountryListContainer countryList = new CountryListContainer();
        countryList.setCountaryDetails(countaryDetails);
        model.addObject("CountryList", countryList);
		model.addObject("title", "My Page");
		model.addObject("message", "Welcome Page");
		model.setViewName("admin");
		return model;

	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Login Form");
		model.addObject("message", "Protected Page");
		model.setViewName("admin");

		return model;

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error, @RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	/*@RequestMapping(value = "/getcountrylist", method = RequestMethod.GET)
    public String getCountryList(Model model) throws Exception{
        List<CountaryDetails> countaryDetails = countryDAOImpl.getListOfCountry();
        CountryListContainer countryList = new CountryListContainer();
        countryList.setCountaryDetails(countaryDetails);
        model.addAttribute("CountryList", countryList);
        return "showCountry";
    }*/
	
}